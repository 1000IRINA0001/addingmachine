package sample.addingMachine;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.addingMachine.animation.Shake;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * класс, демонстрирующий работу арифмометра
 *
 * @author  Прокофьева И.А.
 */
@SuppressWarnings("unused")
public class AddingMachineController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private TextField amount;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;

    /**
     * метод сложения двух целых чисел
     * @param event нажатие кнопки "Сложение"
     */
    @FXML
    void add(ActionEvent event) {
        //если одно из двух чисел не введено, срабатывает анимация
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake number1Anim = new Shake(firstOperand);
            Shake number2Anim = new Shake(secondOperand);
            number1Anim.playAnimation();
            number2Anim.playAnimation();
            return;
        }
        int first = Integer.parseInt(firstOperand.getText());//ввод в поле первого числа
        int second = Integer.parseInt(secondOperand.getText());//ввод в поле второго числа
        int sum = first + second;
        amount.setText(String.valueOf(sum));// занесение результата в поле amount
    }

    /**
     * метод очистки текстовых полей
     * @param event нажатие кнопки "Очистить"
     */
    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        amount.clear();
    }

}